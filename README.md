# moving-kymograph

A Fiji marcro for computing the kymograph along the x axis within the drawn rectangles across frames of a movie. Assuming that the region of interest is moving over time, by drawing the rectangles of the region at some frames. The program will interpolate automatically the region of the remaning frames and produce a kymograph by projecting the signal over the x axis. 

This toolset is used in our paper (cited below) to compute the kymographs from the movies of Atonal of Drosophila eye disc. If you use it successfully for your research please be so kind as to cite our work:

> Pulsatile dynamics propagate crystalline order in the developing Drosophila eye\
> *Lydie Couturier, Juan Luna-Escalante, Khallil Mazouni, Claire Mestdagh, Minh-Son Phan, Jean-Yves Tinevez, François Schweisguth, Francis Corson*\
> bioRxiv 2024.07.11.603179; doi: https://doi.org/10.1101/2024.07.11.603179

The figure below illustrates rectangles drawn in some frames of an example movie **ato_gfp.tif** in the folder **Example**. 

![Ato GFP movie](Medias/ato_gfp.png)

Please use Rectangle tool in Fiji to draw and add to the ROI Manager. Note that, the rectangles in the ROI Manager must be ordered in time. An example of the drawn rectangles for the movie **ato_gfp.tif** is saved to **ato_gfp_rois.zip** in the folder **Example** (you can open this file with ROI Manager).

Before running the marcro, make sure that the movie is opened and the rectangles is in the ROI Manager. There are two parameters you can set:

- **Projection**: support "sum", "avg", "max", "min", "std", "median".
- **Smooth sigma**: sigma value of the Gaussian to smooth the movie.

![GUI](Medias/gui.png)

The figure below is the result by setting projection = "max" and sigma = 2.

![Result](Medias/result.png)

**Remark**
- Do not support multiple channels.
- Only work with projection along the x axis.
- If only one rectangle is drawn, then the kymograph is produced looking at the regions inside the rectangle for all frames.
