# @String (label="Projection", value="max") PROJECTION
# @Float (label="Smooth sigma",min=0,value=2) GAUSS_SIGMA
"""
Compute the kymograph within rectangles. 

Since the rectangle in ImageJ cannot be rotated.
You will need to rotate first the image such that the region where
you compute the kymograph is in xy axes.

The script supports only the kymograph along the x axis for now.

If only one rectangle is drawn, then the kymograph is produced 
looking at the regions within the rectangle across frames.

Otherwise, you can draw several rectangles across time. 
Then the rectangles inbetween frames will be interpolated. 
You must add rectangles to RoiManager iteratively in time, 
e.g. rectangle at frame 1, then frame 10, etc.

Inputs:

- Projection: must be "sum", "avg", "max", "min", "std", "median"
- Smooth sigma: for smoothing the image.


"""
from ij import IJ
from ij.plugin import ZProjector, Concatenator
from ij.plugin.frame import RoiManager
from java.awt import Rectangle
from ij.gui import Roi
from ij.gui import WaitForUserDialog
from ij.measure import ResultsTable

# Orientation alongside or orthogonal
ORIENTATION = 0 # 0: Alongside, 1: Orthogonal, NOTE: only 0 is tested for now.

# Subtract background
RMV_BGR = True

# Get the current active image
img = IJ.getImage() 

# Get the current active RoiManager
roimana = RoiManager().getRoiManager()
roilst = roimana.getRoisAsArray()

if len(roilst)==0: # No rectangle was added to ROI
	# Crop images across frames
	img2 = img.crop("stack");
	img2.setTitle("Crop-"+img.getTitle());
	img2.show();

elif len(roilst)==1: # Only one rectangle in ROI
	roi1 = roilst[0]
	ix1 = roimana.getIndex(roi1.getName())
	roimana.select(img,ix1);
	img2 = img.crop("stack");
	img2.setTitle("Crop-"+img.getTitle());
	img2.show();
	
else: # Multi-Rectangles
	
	# List of cropped images
	imglst = []
	
	# Crop images following rectangles
	for ir in range(len(roilst)):
		
		# Add the first roi to imglst
		roi1 = roilst[ir]
		ix1 = roimana.getIndex(roi1.getName())
		roimana.select(img,ix1);
		imglst.append(img.crop());
		
		if ir == len(roilst)-1:
			break
	
		fr1 = roi1.getPosition(); # Get frame
		print fr1
		rect1 = roi1.getBounds() # Get rectangle
		x1 = int(rect1.getCenterX()) # Get x center
		y1 = int(rect1.getCenterY()) # Get y center
		print "Reference frame forward:",fr1, x1, y1
		
		roi2 = roilst[ir+1]
		fr2 = roi2.getPosition(); # Get frame
		rect2 = roi2.getBounds() # Get rectangle
		x2 = int(rect2.getCenterX()) # Get x center
		y2 = int(rect2.getCenterY()) # Get y center
		print "Reference frame backward:",fr2, x2, y2
		
		for fr in range(fr1+1,fr2):
		
			# Amount of x displacement
			dx = (float(x2-x1)/float(fr2-fr1))*float(fr-fr1)
			x = int(x1 + dx)
			
			# Amount of y displacement
			dy = (float(y2-y1)/float(fr2-fr1))*float(fr-fr1)
			y = int(y1 + dy)
			
			print "> Estimated center at frame {}: {}".format(fr, x, y)
			
			# New Roi
			newx = int(rect1.getX() + dx)
			newy = int(rect1.getY() + dy)
			newrect = Rectangle(newx,newy,int(rect1.getWidth()),int(rect1.getHeight()))
			newroi = Roi(newrect)
			newroi.setPosition(fr)
			imglst.append(img.crop([newroi])[0])
	
	# Concatenate all crop images
	img2 = Concatenator().concatenate(imglst,False);
	img2.setTitle("Crop-"+img.getTitle());
	img2.show();

# If smoothing
if GAUSS_SIGMA > 0:
	IJ.run(img2,"Gaussian Blur...","sigma="+str(GAUSS_SIGMA)+" scaled stack");

# If background subtraction
if RMV_BGR:
	for ifr in range(1,img2.getNFrames()+1):
		img2.setT(ifr);
		IJ.run(img2,"Measure","");
	
	restbl = ResultsTable.getActiveTable();
	for ifr in range(1,img2.getNFrames()+1):
		img2.setT(ifr);
		med = restbl.getValue("Median", ifr-1);
		IJ.run(img2, "Subtract...", "value="+str(med)+" slice");
	
	restbl.reset();

# Swap the axes (XYT => XTY)
if ORIENTATION == 0:
	IJ.run(img2, "Reslice [/]...", "start=Top avoid");
else:
	IJ.run(img2, "Reslice [/]...", "start=Left avoid");

img3 = IJ.getImage();
img3.setTitle("Resliced");

# Max z projection
img4 = ZProjector().run(img3,PROJECTION);
#IJ.run(img4,"16_colors","");
IJ.run(img4,"Fire","");
img4.setTitle("Kymograph-"+img.getTitle());

# Remove shot noise
IJ.run(img4,"Despeckle","");
img4.show();

# Close unneccessary images
img2.changes = False
img2.close();
img3.close();

IJ.selectWindow("Results"); 
IJ.run("Close");